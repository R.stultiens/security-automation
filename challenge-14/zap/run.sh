#!/bin/bash

CMD_BASE="python /zap"

if [ -z $ZAP_TARGET_URL ]; then
    echo "ZAP_TARGET_URL must be defined."
    exit 1
fi

if [ -z $ELK_USER ]; then
    echo "ELK_USER must be defined."
    exit 1
fi

if [ -z $ELK_PASS ]; then
    echo "ELK_PASS must be defined."
    exit 1
fi

if [ -z $ELK_URL ]; then
    echo "ELK_URL must be defined."
    exit 1
fi

# If neither full nor API scan is specified, execute a baseline scan 
if [ $ZAP_SCAN_TYPE == "full" ]; then
    ZAP_CMD="${CMD_BASE}/zap-full-scan.py"
elif [ $ZAP_SCAN_TYPE == "api" ]; then
    ZAP_CMD="${CMD_BASE}/zap-api-scan.py -f ${API_FORMAT}"
    ZAP_TARGET_URL="${ZAP_TARGET_URL}${API_SPEC_PATH}"
else
    ZAP_CMD="${CMD_BASE}/zap-baseline.py"
fi

$ZAP_CMD -t $ZAP_TARGET_URL -J zap_report.json

sleep 5

curl  --user ${ELK_USER}:${ELK_PASS} -XPOST ${ELK_URL}/zap_scanner/_doc -d @/zap/wrk/zap_report.json -H 'Content-Type: application/json'

#optional https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-create-api-key.html
# An other option is to use filebeats and the elastic cloud ID for authentication